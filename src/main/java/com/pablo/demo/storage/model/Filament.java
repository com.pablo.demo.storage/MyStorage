package com.pablo.demo.storage.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;


@Entity
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@ToString
public class Filament {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //name of filament producer
    @NonNull
    private String brand;

    //type of filament material
    @NonNull
    private String material;

    //color of filament
    @NonNull
    private String color;

    //actual weight of filament with spool in grams (integer)
    @NonNull
    private int weight;

    //weight of filament spool in grams (integer)
    @NonNull
    private int spool;

    //filament price in CZK/kg (two decimal)
    @NonNull
    private double price;

    //date of filament insertion into storage
    @NonNull
    private Date buyDate;

}
