package com.pablo.demo.storage.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilamentDTO {
    private int id;
    private String brand;
    private String material;
    private String color;
    private int weight;
    private int spool;
    private double price;
    private Date buyDate;
}
