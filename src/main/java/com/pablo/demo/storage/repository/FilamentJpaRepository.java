package com.pablo.demo.storage.repository;

import com.pablo.demo.storage.model.Filament;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilamentJpaRepository extends JpaRepository<Filament, Integer> {
    List<Filament> findByBrand (String brand);
}
