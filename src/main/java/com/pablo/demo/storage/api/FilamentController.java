package com.pablo.demo.storage.api;

import com.pablo.demo.storage.model.Filament;
import com.pablo.demo.storage.model.FilamentDTO;
import com.pablo.demo.storage.service.FilamentService;
import io.swagger.v3.oas.annotations.Operation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RequestMapping("api/filaments")
@RestController
public class FilamentController {

    private final FilamentService filamentService;

    private final ModelMapper modelMapper;

    @Autowired
    public FilamentController(FilamentService filamentService, ModelMapper modelMapper) {
        this.filamentService = filamentService;
        this.modelMapper = modelMapper;
    }

    @Operation(summary = "This shows the list of all filaments")
    @GetMapping
    public List<Filament> getAllFilaments() {
        return filamentService.getAllFilaments();
    }

    @Operation(summary = "Add filament")
    @PostMapping
    public void addFilament(@RequestBody FilamentDTO filamentDto) {
        Filament filament = this.modelMapper.map(filamentDto, Filament.class);
        filamentService.addFilament(filament);
    }

    @Operation(summary = "Update filament")
    @PutMapping
    public void updateFilament(@RequestBody Filament filamentDto) {
        Filament filament = this.modelMapper.map(filamentDto, Filament.class);
        filamentService.updateFilament(filament);
    }

    @Operation(summary = "Delete filament by ID")
    @DeleteMapping(path = "{id}")
    public void deleteFilamentById(@PathVariable("id") int id) {
        Optional<Filament> filament = filamentService.findFilamentByID(id);
        if (filament.isEmpty()){
            throw new ResponseStatusException(NOT_FOUND,
                    "Cannot delete filament with non-existent ID");
        }
        else {
            filamentService.deleteFilamentById(id);
        }
    }

    @Operation(summary = "Show filament by ID")
    @GetMapping(path = "{id}")
    public Optional<Filament> findFilamentById(@PathVariable("id") int id) {
        Optional<Filament> filament = filamentService.findFilamentByID(id);
        if (filament.isEmpty()){
            throw new ResponseStatusException(NOT_FOUND,
                    "Cannot show filament with non-existent ID");
        }
        else{
            return filament;
        }
    }

    @Operation(summary = "Show filament by brand")
    @GetMapping(path = "brand/{brand}")
    public List<Filament> findFilamentByBrand(@PathVariable("brand") String brand){
        List<Filament> filamentList = filamentService.findFilamentByBrand(brand);
        if (filamentList.isEmpty()){
            throw new ResponseStatusException(NOT_FOUND,
                    "Cannot show filaments with non-existent BRAND");
        }
        else{
            return filamentService.findFilamentByBrand(brand);
        }
    }

    @Operation(summary = "Show filament actual weight by ID")
    @GetMapping(path = "weight/{id}")
    public String showActualWeightById(@PathVariable("id") int id){
        Optional<Filament> filament = filamentService.findFilamentByID(id);
        if (filament.isEmpty()){
            throw new ResponseStatusException(NOT_FOUND,
                    "Cannot show weight of filament with non-existent ID");
        }
        else{
            return String.format("Actual weight of filament with ID %d is %d grams", id,
                    filamentService.calculateActualWeight(id));
        }
    }

    @Operation(summary = "Show filament age by ID")
    @GetMapping(path = "age/{id}")
    public String showAgeById(@PathVariable("id") int id){
        Optional<Filament> filament = filamentService.findFilamentByID(id);
        if (filament.isEmpty()){
            throw new ResponseStatusException(NOT_FOUND,
                    "Cannot show age of filament with non-existent ID");
        }
        else{
            return String.format("Actual age of filament with ID %d is %d year/s (%d days)", id,
                    filamentService.calculateAgeInYears(id), filamentService.calculateAgeInDays(id));
        }
    }

    @Operation(summary = "Subtract filament weight")
    @PostMapping(path = "/sub")
    public String subsWeight(@RequestParam("id") int id, @RequestParam("weight") int weight){
        Optional<Filament> filament = filamentService.findFilamentByID(id);
        int subs = filamentService.subsWeight(id, weight);
        if (filament.isEmpty()){
            throw new ResponseStatusException(NOT_FOUND,
                    "Cannot subtract weight of filament with non-existent ID");
        } else if ( subs == 0 ){
            throw new ResponseStatusException(BAD_REQUEST, "Invalid WEIGHT value");
        }
        else{
            return String.format("Actual weight of filament with ID %d is %d grams (%dg without spool)",
                    id, subs,
                    filamentService.calculateActualWeight(id));
        }
    }

}
