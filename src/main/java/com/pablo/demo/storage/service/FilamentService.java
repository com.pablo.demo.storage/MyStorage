package com.pablo.demo.storage.service;

import com.pablo.demo.storage.model.Filament;
import com.pablo.demo.storage.repository.FilamentJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class FilamentService {

    private final FilamentJpaRepository jpaRepository;

    @Autowired
    public FilamentService(FilamentJpaRepository jpaRepository) {
        this.jpaRepository = jpaRepository;
    }

    public List<Filament> getAllFilaments() {return jpaRepository.findAll();}

    public Filament addFilament(Filament filament) {
        return jpaRepository.save(filament);}

    public Filament updateFilament(Filament filament) {
        return jpaRepository.save(filament);}

    public void deleteFilamentById(int id){
        jpaRepository.deleteById(id);
    }

    public Optional<Filament> findFilamentByID(int id){
        return jpaRepository.findById(id);
    }

    public List<Filament> findFilamentByBrand(String brand){
        return jpaRepository.findByBrand(brand);
    }

    public int calculateActualWeight(int id){
        return jpaRepository.getById(id).getWeight()-jpaRepository.getById(id).getSpool();
    }

    public int calculateAgeInDays(int id){
        long diff = (new Date().getTime()-jpaRepository.getById(id).getBuyDate().getTime())
                /(1000*60*60*24);
        return (int) diff;
    }

    public int calculateAgeInYears(int id){
        long diff = (new Date().getTime()-jpaRepository.getById(id).getBuyDate().getTime())
                /(1000*60*60*24)/365;
        return (int) diff;
    }

    public int subsWeight(int id, int weight){
        if ( weight <= 0 || calculateActualWeight(id)-weight <= 0 ){
            return 0;
        } else {
            int myWeight = jpaRepository.getById(id).getWeight() - weight;
            jpaRepository.save(new Filament(
                    jpaRepository.getById(id).getId(),
                    jpaRepository.getById(id).getBrand(),
                    jpaRepository.getById(id).getMaterial(),
                    jpaRepository.getById(id).getColor(),
                    myWeight,
                    jpaRepository.getById(id).getSpool(),
                    jpaRepository.getById(id).getPrice(),
                    jpaRepository.getById(id).getBuyDate())
            );
            return myWeight;
        }
    }

}
