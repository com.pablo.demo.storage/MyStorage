INSERT INTO FILAMENT (brand, material, color, weight, spool, price, buy_date)
VALUES ('Prusament', 'PLA', 'green', 400, 150, 799.99, '2020-12-17');
INSERT INTO FILAMENT (brand, material, color, weight, spool, price, buy_date)
VALUES ('Prusament', 'PETG', 'yellow', 650, 150, 799.99, '2021-10-12');
INSERT INTO FILAMENT (brand, material, color, weight, spool, price, buy_date)
VALUES ('EkoMB', 'PETG', 'red', 340, 110, 499.99, '2020-10-12');
INSERT INTO FILAMENT (brand, material, color, weight, spool, price, buy_date)
VALUES ('EkoMB', 'PLA', 'black', 620, 110, 499.99, '2020-02-22');
INSERT INTO FILAMENT (brand, material, color, weight, spool, price, buy_date)
VALUES ('Creality', 'PLA', 'white', 970, 75, 399.99, '2022-02-20');