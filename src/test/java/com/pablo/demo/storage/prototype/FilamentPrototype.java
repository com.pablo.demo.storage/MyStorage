package com.pablo.demo.storage.prototype;

import com.pablo.demo.storage.model.Filament;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FilamentPrototype {

    public static Filament aFilament(){
        return new Filament("Prusament", "PETG", "red",
                450, 215, 899.99, new Date(120,11,12));
    }

    public static Filament bFilament(){
        return new Filament("Prusament", "PLA", "blue",
                720, 215, 799.99, new Date(118, 7,3));
    }

    public static Filament cFilament(){
        return new Filament("EkoMB", "rPLA", "white",
                220, 150, 549, new Date(118, 5,3));
    }

    public static List<Filament> filamentList(){
        List<Filament> fList = new ArrayList<>();
        fList.add(new Filament(1,"Prusament", "PETG", "red",
                        450, 215, 899.99, new Date(120,11,12)));
        fList.add(new Filament(2,"Prusament", "PLA", "blue",
                720, 215, 799.99, new Date(118, 7,3)));
        return fList;
    }
}
