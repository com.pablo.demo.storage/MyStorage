package com.pablo.demo.storage.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pablo.demo.storage.model.Filament;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.pablo.demo.storage.prototype.FilamentPrototype.*;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class FilamentJpaRepositoryTest {

    @Autowired
    private FilamentJpaRepository filamentRepository;

    @Test
    void findByBrand() throws JsonProcessingException {
        filamentRepository.save(aFilament());
        filamentRepository.save(bFilament());
        filamentRepository.save(cFilament());
        ObjectMapper objectMapper = new ObjectMapper();
        List<Filament> foundFilamentList = filamentRepository.findByBrand(aFilament().getBrand());
        assertFalse(foundFilamentList.isEmpty());
        assertEquals("Prusament",foundFilamentList.get(aFilament().getId()).getBrand());
        assertEquals(filamentList().get(0).getBrand(),foundFilamentList.get(0).getBrand());
        assertEquals(filamentList().get(1).getMaterial(),foundFilamentList.get(1).getMaterial());
        assertEquals(objectMapper.writeValueAsString(filamentList()),
                objectMapper.writeValueAsString(foundFilamentList));
    }
}