package com.pablo.demo.storage;

import com.pablo.demo.storage.api.FilamentController;
import com.pablo.demo.storage.service.FilamentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class MyStorageApplicationTests {

	@Autowired
	private FilamentController filamentController;

	@Autowired
	private FilamentService filamentService;

	@Test
	void contextLoads() throws Exception {
		assertThat(filamentController).isNotNull();
		assertThat(filamentService).isNotNull();
	}

}
