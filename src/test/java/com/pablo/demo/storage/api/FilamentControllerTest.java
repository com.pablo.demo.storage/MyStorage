package com.pablo.demo.storage.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pablo.demo.storage.service.FilamentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static com.pablo.demo.storage.prototype.FilamentPrototype.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class FilamentControllerTest {

    MockMvc mockMvc;
    ObjectMapper objectMapper;
    FilamentService filamentService;
    ModelMapper modelMapper;

    @BeforeEach
    void setUp(){
        filamentService = mock(FilamentService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(new FilamentController(filamentService, modelMapper)).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void getAllFilaments() throws Exception {
        when(filamentService.getAllFilaments()).thenReturn(filamentList());
        mockMvc.perform(get("/api/filaments")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(filamentList())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(filamentList())));
    }

    @Test
    void addFilament() throws Exception {
        mockMvc.perform(post("/api/filaments/"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateFilament() throws Exception {
        mockMvc.perform(put("/api/filaments/"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteFilamentById() throws Exception {
        mockMvc.perform(delete("/api/filaments/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void findFilamentById() throws Exception {
        when(filamentService.findFilamentByID(anyInt())).thenReturn(Optional.of(aFilament()));
        mockMvc.perform(get("/api/filaments/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(aFilament())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(aFilament())));
    }

    @Test
    void findFilamentByBrand() throws Exception {
        when(filamentService.findFilamentByBrand(anyString())).thenReturn(filamentList());
        mockMvc.perform(get("/api/filaments/brand/Prusament")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(filamentList())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(filamentList())));
    }

    @Test
    void showActualWeightById() throws Exception {
        mockMvc.perform(get("/api/filaments/weight/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void showAgeById() throws Exception {
        mockMvc.perform(get("/api/filaments/age/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void subsWeight() throws Exception{
        mockMvc.perform(get("/api/filaments/subs"))
                .andExpect(status().isBadRequest());
    }
}