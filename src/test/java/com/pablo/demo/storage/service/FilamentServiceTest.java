package com.pablo.demo.storage.service;

import com.pablo.demo.storage.model.Filament;
import com.pablo.demo.storage.repository.FilamentJpaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.pablo.demo.storage.prototype.FilamentPrototype.aFilament;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FilamentServiceTest {

    private FilamentJpaRepository filamentRepository;
    private FilamentService filamentService;

    @BeforeEach
    void setUp(){
        filamentRepository = mock(FilamentJpaRepository.class);
        filamentService = new FilamentService(filamentRepository);
    }

    @Test
    void addFilament() {
        when(filamentService.addFilament(any())).thenReturn(aFilament());
        Filament createdFilament = filamentService.addFilament(aFilament());
        assertEquals("Prusament",createdFilament.getBrand());
    }

    @Test
    void updateFilament() {
        when(filamentService.updateFilament(any())).thenReturn(aFilament());
        Filament createdFilament = filamentService.updateFilament(aFilament());
        assertEquals("Prusament",createdFilament.getBrand());
    }

    @Test
    void calculateActualWeight() {
        when(filamentRepository.getById(any())).thenReturn(aFilament());
        int result = filamentService.calculateActualWeight(aFilament().getId());
        assertEquals(235,result);
    }

    @Test
    void calculateAgeInDays() {
        when(filamentRepository.getById(any())).thenReturn(aFilament());
        int result = filamentService.calculateAgeInDays(aFilament().getId());
        assertEquals(438,result);
    }

    @Test
    void calculateAgeInYears() {
        when(filamentRepository.getById(any())).thenReturn(aFilament());
        int result = filamentService.calculateAgeInYears(aFilament().getId());
        assertEquals(1,result);
    }

    @Test
    void subsWeight() {
        when(filamentRepository.getById(any())).thenReturn(aFilament());
        int result = filamentService.subsWeight(aFilament().getId(),50);
        assertEquals(400,result);
    }
}